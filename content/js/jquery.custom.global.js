/* 
	NAVIGATION: this section either displays or hides the navigation.

	We need to hide the menu when the user leaves the menu but may
	not have escaped the containing div that spans the width of the window.
	
	Hiding the tab happens when the user leaves the nav-container-X DIV, the containing
	parent UL or the grandpartent DIV.
	
*/
    
$(document).ready(function () {

  // myBC Tab
  $("#tab-account").mouseenter(function(){
    $("#nav-container-account").css("display","block");
  });
  $(".nav-account,.navigation,#tab-account").mouseleave(function(){
    $("#nav-container-account").css("display","none");
  });

  // myBC Tab
  $("#tab-mybc").mouseenter(function(){
    $("#nav-container-mybc").css("display","block");
  });
  $(".nav-mbc,.navigation,#tab-mybc").mouseleave(function(){
    $("#nav-container-mybc").css("display","none");
  });


  // Plans Tab
  $("#tab-plans").mouseenter(function(){
    $("#nav-container-plans").css("display","block");
  });
  $(".nav-plans,.navigation,#tab-plans").mouseleave(function(){
    $("#nav-container-plans").css("display","none");
  });

  // Wellness Tab
  $("#tab-wellness").mouseenter(function(){
    $("#nav-container-wellness").css("display","block");
  });
  $(".nav-wellness,.navigation,#tab-wellness").mouseleave(function(){
    $("#nav-container-wellness").css("display","none");
  });

  // Pharmacy Tab
  $("#tab-pharmacy").mouseenter(function(){
    $("#nav-container-pharmacy").css("display","block");
  });
  $(".nav-pharmacy,.navigation,#tab-pharmacy").mouseleave(function(){
    $("#nav-container-pharmacy").css("display","none");
  });

}); // doc ready
// end Navigation

/* 
	
	RIGHT RAIL CONTENT SLIDERS
	
*/

var accountHeight = "600px"; // This adjusts the height of the right rail My Account slider.
var tweetHeight = "100px"; // This adjusts the height of the right rail Twitter slider.

$(document).ready(function(){
	$('.slider1').each(function () {
				var current = $(this);
				current.attr("box_h", current.height());
			}
	 );
	
	$(".slider1").css("height", accountHeight);
	
	$(".slider_menu1").html('<a href="javascript:void(0)" class="expand">More</a>');
	$(".slider_menu1 a").click(function() { openSlider() })
});

function openSlider()
{
	var open_height = $(".slider1").attr("box_h") + "px";
	$(".slider1").animate({"height": open_height}, {duration: "slow" });
	
	$(".slider_menu1").html('<a href="javascript:void(0)" class="condense">Less</a>');
	$(".slider_menu1 a").click(function() { closeSlider() })
}

function closeSlider()
{
	$(".slider1").animate({"height": accountHeight}, {duration: "slow" });
	$(".slider_menu1").html('<a href="javascript:void(0)" class="expand">More</a>');
	$(".slider_menu1 a").click(function() { openSlider() })
}

$(document).ready(function(){
	$('.slider2').each(function () {
				var current = $(this);
				current.attr("box_h", current.height());
			}
	 );
	
	$(".slider2").css("height", tweetHeight);
	$(".slider_menu2").html('<a href="javascript:void(0)" class="expand">More</a>');
	$(".slider_menu2 a").click(function() { openSlider2() })
});

function openSlider2()
{
	var open_height = $(".slider2").attr("box_h") + "px";
	$(".slider2").animate({"height": open_height}, {duration: "slow" });
	$(".slider_menu2").html('<a href="javascript:void(0)" class="condense">Less</a>');
	$(".slider_menu2 a").click(function() { closeSlider2() })
}

function closeSlider2()
{
	$(".slider2").animate({"height": tweetHeight}, {duration: "slow" });
	$(".slider_menu2").html('<a href="javascript:void(0)" class="expand">More</a>');
	$(".slider_menu2 a").click(function() { openSlider2() })
}

/*
	JQ Transform

*/

$(function(){
		$("form.jqtransform").jqTransform();
	});
	// anything slider
	function onBlur(el) {
    if (el.value == '') {
        el.value = el.defaultValue;
    }
}
function onFocus(el) {
    if (el.value == el.defaultValue) {
        el.value = '';
    }
}
    
        function formatText(index, panel) {
		  return index + "";
	    }
    
        $(function () {
          $("#1, #2, #3").lavaLamp({
                speed: 700,
                click: function(event, menuItem) {
                    return menuItem;
                }
            });
        
            $('.anythingSlider').anythingSlider({
                easing: "easeInOutExpo",        // Anything other than "linear" or "swing" requires the easing plugin
                autoPlay: true,                 // This turns off the entire FUNCTIONALY, not just if it starts running or not.
                delay: 6000,                    // How long between slide transitions in AutoPlay mode
                startStopped: false,            // If autoPlay is on, this can force it to start stopped
                animationTime: 800,             // How long the slide transition takes
                hashTags: true,                 // Should links change the hashtag in the URL?
                buildNavigation: true,          // If true, builds and list of anchor links to link to each slide
        		pauseOnHover: true,             // If true, and autoPlay is enabled, the show will pause on hover
        		startText: "",             // Start text
		        stopText: "",               // Stop text
		        navigationFormatter: null       // Details at the top of the file on this use (advanced use)
            });
            
            $("#slide-jump").click(function(){
                $('.anythingSlider').anythingSlider(6);
            });
            
        });
        
        /*

	COLOR BOX: We implement the color box in the parent window
	so the color box in the child page escapes it's bounds
	and displays in the full page.

*/ 
 
  $(document).ready(function () {
 
// ColorBox: This makes the link inside the iframe open in parent window
    $('a.colorbox').click(function (event) {
        event.preventDefault(); // this just cancels the default link behavior.
        parent.showColorBox($(this).attr("href")); //this makes the parent window load the showColorBox function, using the a.colorbox href value
    });
});
 
// ColorBox: This function only needs to be available in the parent window, but no harm in loading it for both. Notice this is NOT in the $(document).ready on purpose.
function showColorBox(imageURL) {
    $.fn.colorbox({ innerWidth: "660px", innerHeight: "350px", iframe: true, transition: "elastic", opacity: .6, open: true, href: imageURL });
}